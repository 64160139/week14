/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.fah.swingproject;

/**
 *
 * @author piigt
 */
public class TestException {

    public static void main(String[] args) {
        try {
            throw new Exception("Hello Exception");
            /*int[] arr = {1, 2, 3};
            System.out.println(arr[0]);
            System.out.println(arr[1]);
            System.out.println(arr[2]);
            System.out.println(arr[3]);
            System.out.println("hello");
            Integer.parseInt("-");*/
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Array Exception");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally{
            System.out.println("Final Exception");
        }

    }

}
