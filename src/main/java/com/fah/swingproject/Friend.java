/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.fah.swingproject;

import java.io.Serializable;

/**
 *
 * @author piigt
 */
public class Friend implements Serializable {
    private String name;
    private int age;
    private String gender;
    private String desciption ;

    public Friend(String name, int age, String gender, String desciption) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.desciption = desciption;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDesciption() {
        return desciption;
    }

    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    @Override
    public String toString() {
        return "Friend{" + "name=" + name + ", age=" + age + ", gender=" + gender + ", desciption=" + desciption + '}';
    }
    
}
